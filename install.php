<?php
BOL_LanguageService::getInstance()->addPrefix('notes', 'User notes');

$sql = "CREATE TABLE `" . OW_DB_PREFIX . "notes_department` (
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `authorId` INTEGER(11) NOT NULL,
    `title` VARCHAR(200) NOT NULL,
    `body` TEXT NOT NULL,
    `timestamp` INTEGER(11) NOT NULL,
    `isDraft` TINYINT(1) NOT NULL,
    `privacy` varchar(50) NOT NULL default 'everybody',
    PRIMARY KEY (`id`)
)
ENGINE=MyISAM DEFAULT CHARSET=utf8
ROW_FORMAT=DEFAULT";

OW::getDbo()->query($sql);