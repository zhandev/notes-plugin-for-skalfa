<?php
OW::getRouter()->addRoute(new OW_Route('notes.index', 'notes', "NOTES_CTRL_Note", 'index'));
OW::getRouter()->addRoute(new OW_Route('notes.create', 'notes/create', "NOTES_CTRL_Note", 'create'));
OW::getRouter()->addRoute(new OW_Route('notes.manage', 'notes/manage', "NOTES_CTRL_Note", 'manage'));
OW::getRouter()->addRoute(new OW_Route('notes.main', 'notes/:action', "NOTES_CTRL_Note", 'index'));
OW::getRouter()->addRoute(new OW_Route('notes.delete', 'notes/delete/:id', "NOTES_CTRL_Note", 'delete'));