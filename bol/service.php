<?php
/**
 * Created by PhpStorm.
 * User: zhan
 * Date: 28.02.15
 * Time: 20:51
 */
class NOTES_BOL_Service
{
    /**
     * Singleton instance.
     *
     * @var NOTES_BOL_Service
     */
    private static $classInstance;

    /**
     * Returns an instance of class (singleton pattern implementation).
     *
     * @return NOTES_BOL_Service
     */
    public static function getInstance()
    {
        if (self::$classInstance === null) {
            self::$classInstance = new self();
        }

        return self::$classInstance;
    }

    private function __construct()
    {

    }

    public function getDepartmentLabel($id)
    {
        return OW::getLanguage()->text('notes', $this->getDepartmentKey($id));
    }

    public function addDepartment($title, $body, $authorId,$timestamp, $isDraft, $privacy, $label = null)
    {
        $note = new NOTES_BOL_Department();
        $note->title = $title;
        $note->body = $body;
        $note->authorId = $authorId;
        $note->isDraft = $isDraft;
        $note->timestamp = $timestamp;
        $note->privacy = $privacy;

        NOTES_BOL_DepartmentDao::getInstance()->save($note);

        BOL_LanguageService::getInstance()->addValue(
            OW::getLanguage()->getCurrentId(),
            'notes',
            $this->getDepartmentKey($note->id),
            trim($label));
    }

    public function deleteDepartment($id)
    {
        $id = (int)$id;
        if ($id > 0) {
            $key = BOL_LanguageService::getInstance()->findKey('notes', $this->getDepartmentKey($id));
            BOL_LanguageService::getInstance()->deleteKey($key->id, true);
            NOTES_BOL_DepartmentDao::getInstance()->deleteById($id);
        }
    }

    private function getDepartmentKey($name)
    {
        return 'dept_' . trim($name);
    }

    public function getDepartmentList()
    {
        return NOTES_BOL_DepartmentDao::getInstance()->findAll();
    }

    public function getCountNotesForUser($authorId)
    {
        return NOTES_BOL_DepartmentDao::getInstance()->getCountNotesForUser($authorId);
    }

    public function getAuthorIds()
    {
        $service = NOTES_BOL_DepartmentDao::getInstance();
        return $service->getAuthorIds();
    }

    public function getAuthorNotesForPaginate($first, $count, $authorId)
    {
        return NOTES_BOL_DepartmentDao::getInstance()->getAuthorNotesForPaginate($first, $count, $authorId);

    }

    public function getNotesForPaginate($first, $count)
    {
        return NOTES_BOL_DepartmentDao::getInstance()->getNotesForPaginate($first, $count);
    }
}