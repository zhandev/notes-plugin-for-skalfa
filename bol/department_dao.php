<?php
/**
 * Created by PhpStorm.
 * User: zhan
 * Date: 28.02.15
 * Time: 20:51
 */
class NOTES_BOL_DepartmentDao extends OW_BaseDao
{

    /**
     * Constructor.
     *
     */
    protected function __construct()
    {
        parent::__construct();
    }
    /**
     * Singleton instance.
     *
     * @var CONTACTUS_BOL_DepartmentDao
     */
    private static $classInstance;

    /**
     * Returns an instance of class (singleton pattern implementation).
     *
     * @return NOTES_BOL_DepartmentDao
     */
    public static function getInstance()
    {
        if ( self::$classInstance === null )
        {
            self::$classInstance = new self();
        }

        return self::$classInstance;
    }

    /**
     * @see OW_BaseDao::getDtoClassName()
     *
     */
    public function getDtoClassName()
    {
        return 'NOTES_BOL_Department';
    }

    /**
     * @see OW_BaseDao::getTableName()
     *
     */
    public function getTableName()
    {
        return OW_DB_PREFIX . 'notes_department';
    }

    public function getAuthorIds($cacheLifeTime = 0, $tags = array() )
    {
        $sql = 'SELECT DISTINCT `authorId` FROM ' . $this->getTableName();

        return $this->dbo->queryForColumnList($sql, array(), $cacheLifeTime, $tags);
    }

    public function getAuthorNotesForPaginate($first, $count, $authorId, $cacheLifeTime = 0, $tags = array() )
    {
        $sql = 'SELECT * FROM ' . $this->getTableName() . ' WHERE `authorId` = ?' . ' LIMIT ' . (int) $first . ', ' . (int) $count;

        return $this->dbo->queryForObjectList($sql, $this->getDtoClassName(), array((int) $authorId), $cacheLifeTime, $tags);
    }

    public function getNotesForPaginate($first, $count, $cacheLifeTime = 0, $tags = array())
    {
        $sql = 'SELECT * FROM ' . $this->getTableName() .' ORDER BY timestamp DESC' . ' LIMIT ' . (int) $first . ', ' . (int) $count;
        return $this->dbo->queryForObjectList($sql, $this->getDtoClassName(), array(), $cacheLifeTime, $tags);
    }

    public function getCountNotesForUser( $authorId, $cacheLifeTime = 0, $tags = array() )
    {
        $sql = 'SELECT * FROM ' . $this->getTableName() . ' WHERE `authorId` = ?';

        return $this->dbo->queryForObjectList($sql, $this->getDtoClassName(), array((int) $authorId), $cacheLifeTime, $tags);
    }
}