<?php

class NOTES_CTRL_Note extends OW_ActionController
{
    public function index()
    {
        /**
         * Set information about page
         */
        $this->setPageTitle("Notes");
        $this->setPageHeading("Notes");


        $page = (!empty($_GET['page']) && intval($_GET['page']) > 0 ) ? $_GET['page'] : 1;



        /**
         * Plugin menu
         */
        $contentMenu = $this->getContentMenu();
        $contentMenu->getElement('index')->setActive(true);
        $this->addComponent('menu', $contentMenu );


        /**
         * Pagination
         */
        $notesOnPage = 5;
        $allNotes = $this->getNotes($action = 'index', $page, $notesOnPage);
        $itemCount = count(NOTES_BOL_Service::getInstance()->getDepartmentList());
        $this->addComponent('paging', new BASE_CMP_Paging($page, ceil($itemCount / $notesOnPage), 5));


        /*
         * Display user information
         */
        $this->assign('userUrlsList', $this->getUserUrlList());
        $this->assign('userNamesList', $this->getUserNameList());
        $this->assign('avatars', $this->getAvatars());

        /**
         * Display notes with paging
         */
        $this->assign('notes', $allNotes);
        $this->assign('notesCount', $itemCount);
    }

    public function manage()
    {
        /**
         * Set auth
         */
        if ( !OW::getUser()->isAuthenticated() )
        {
            throw new AuthenticateException();
        }

        /**
         * Set information about page
         */
        $this->setPageTitle("My notes");
        $this->setPageHeading("My notes");

        $page = (!empty($_GET['page']) && intval($_GET['page']) > 0 ) ? $_GET['page'] : 1;

        /**
         * Plugin menu
         */
        $contentMenu = $this->getContentMenu();
        $this->addComponent('menu', $contentMenu );

        /*
         * Get user notes
         */
        $notesOnPage = 5;
        $allNotes = $this->getNotes($action = 'manage', $page, $notesOnPage);
        $itemCount = count(NOTES_BOL_Service::getInstance()->getCountNotesForUser(OW::getUser()->getId()));
        $this->addComponent('paging', new BASE_CMP_Paging($page, ceil($itemCount / $notesOnPage), 5));

        /*
         * Display user information
         */
        $this->assign('userUrlsList', $this->getUserUrlList());
        $this->assign('userNamesList', $this->getUserNameList());
        $this->assign('avatars', $this->getAvatars());

        /**
         * Display notes with paging
         */

        $this->assign('notes', $allNotes);
    }

    public function create()
    {
        /**
         * Set auth
         */
        if ( !OW::getUser()->isAuthenticated() )
        {
            throw new AuthenticateException();
        }

        /**
         * Set information about page
         */
        $this->setPageTitle("Create note");
        $this->setPageHeading("Create note");

        /**
         * Plugin menu
         */
        $contentMenu = $this->getContentMenu();
        $this->addComponent('menu', $contentMenu );

        /**
         * Add form
         */
        $form = new Form('note_form');

        $fieldTitle = new TextField('title');
        $fieldTitle->setLabel($this->text('notes', 'form_label_title'));
        $fieldTitle->setRequired();
        $form->addElement($fieldTitle);

        $fieldBody = new Textarea('body');
        $fieldBody->setLabel($this->text('notes', 'form_label_body'));
        $fieldBody->setRequired();
        $textareaValidator = new StringValidator(1, 35);
        $fieldBody->addValidator($textareaValidator);
        $form->addElement($fieldBody);

        $submit = new Submit('send');
        $submit->setValue('Save note');
        $form->addElement($submit);

        $this->addForm($form);

        /**
         * Save note
         */
        if( OW::getRequest()->isPost() )
        {
            if( $form->isValid($_POST) )
            {


                $values = $form->getValues();
                $values['authorId'] = OW::getUser()->getId();
                $values['timestamp'] = time();
                $values['isDraft'] = 2;
                $values['privacy'] = "everybody";

                $saveStatus = NOTES_BOL_Service::getInstance()->addDepartment($values['title'], $values['body'], $values['authorId'], $values['timestamp'], $values['isDraft'], $values['privacy']);

                OW::getFeedback()->info(OW::getLanguage()->text('notes', 'create_draft_success_msg'));
                $this->redirect(OW::getRouter()->urlForRoute('notes.index'));
            }
        }
    }


    /**
     * Example delete action
     */
//    public function delete( $params )
//    {
//        if ( !OW::getUser()->isAuthenticated() )
//        {
//            throw new AuthenticateException();
//        }
//        $id = $params['id'];
//        NOTES_BOL_Service::getInstance()->deleteDepartment( $params );
//    }

    private function getContentMenu()
    {
        $menuItems = array();

        $listNames = array(
            'create' => array('iconClass' => 'ow_ic_tag'),
            'manage' => array('iconClass' => 'ow_ic_star'),
            'index' => array('iconClass' => 'ow_ic_clock'),
        );
        if ( !OW::getUser()->isAuthenticated() )
        {
            unset($listNames['manage']);
            unset($listNames['create']);
        }

        foreach ( $listNames as $listKey => $listArr )
        {
            $menuItem = new BASE_MenuItem();
            $menuItem->setKey($listKey);
            $menuItem->setUrl(OW::getRouter()->urlForRoute('notes.main', array('action' => $listKey)));
            $menuItemKey = explode('-', $listKey);
            $listKey = "";
            foreach ($menuItemKey as $key)
            {
                $listKey .= strtoupper(substr($key, 0, 1)).substr($key, 1);
            }

            $menuItem->setLabel(OW::getLanguage()->text('notes', "menuItem".$listKey));
            $menuItem->setIconClass($listArr['iconClass']);
            $menuItems[] = $menuItem;
        }

        return new BASE_CMP_ContentMenu($menuItems);
    }


    private function text( $prefix, $key, array $vars = null )
    {
        return OW::getLanguage()->text($prefix, $key, $vars);
    }

    private function getNotes($action = null, $page, $notesOnPage)
    {
        /*
         * Get notes
         */
        $allNotes = array();

        $first = $page;
        if($action == 'index') {
            $notes = NOTES_BOL_Service::getInstance()->getNotesForPaginate(($first * $notesOnPage - $notesOnPage), $notesOnPage);
        }else if($action == 'manage'){
            $notes = NOTES_BOL_Service::getInstance()->getAuthorNotesForPaginate(($first * $notesOnPage - $notesOnPage), $notesOnPage, OW::getUser()->getId());
        }

        foreach ( $notes as $note )
        {
            $allNotes[$note->id]['id'] = $note->id;
            $allNotes[$note->id]['title'] = $note->title;
            $allNotes[$note->id]['body'] = $note->body;
            $allNotes[$note->id]['authorId'] = $note->authorId;
            $allNotes[$note->id]['timestamp'] = UTIL_DateTime::formatDate($note->timestamp);
        }

        return $allNotes;
    }

    private function getUserNameList()
    {
        $userService = BOL_UserService::getInstance();
        $ids = NOTES_BOL_Service::getInstance()->getAuthorIds();

        return $userService->getDisplayNamesForList($ids);
    }

    private function getUserUrlList()
    {
        $userService = BOL_UserService::getInstance();
        $ids = NOTES_BOL_Service::getInstance()->getAuthorIds();

        return $userService->getUserUrlsForList($ids);
    }

    private function getAvatars()
    {
        $userService = BOL_UserService::getInstance();
        $ids = NOTES_BOL_Service::getInstance()->getAuthorIds();

        return BOL_AvatarService::getInstance()->getDataForUserAvatars($ids, true, false);
    }
}